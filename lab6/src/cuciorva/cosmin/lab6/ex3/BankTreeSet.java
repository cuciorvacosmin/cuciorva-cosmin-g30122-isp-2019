package cuciorva.cosmin.lab6.ex3;

import cuciorva.cosmin.lab6.ex1.BankAccount;

import java.util.ArrayList;
import java.util.TreeSet;

public class BankTreeSet extends BankAccount {
    TreeSet<BankAccount> ownerComp = new TreeSet<BankAccount>(new MyNameComparator());
    TreeSet<BankAccount> balanceComp = new TreeSet<BankAccount>(new MyBalanceComparator());


    public void printAccounts() {
        for (BankAccount bank : balanceComp) {
            System.out.println(bank.getOwner() + " " + bank.getBalance());
        }
    }


    public String getAllAccounts() {
        for (BankAccount bank : ownerComp) {
            return bank.getOwner() + " " + bank.getBalance();
        }
        return null;
    }

    public String getAccount(String owner) {
//        Iterator<BankAccount> value = ownerComp.iterator();
        ArrayList<BankAccount> bankAccounts = new ArrayList<BankAccount>(ownerComp);
        for (int i = 0; i < bankAccounts.size(); i++) {
            if (bankAccounts.get(i).getOwner() == owner) {
                return bankAccounts.get(i).getOwner() + " " + bankAccounts.get(i).getBalance();
            }
        }
        return null;
    }

    public void addAccount(String owner, double balance) {
        BankAccount auxBankAcc = new BankAccount(owner, balance);
        ownerComp.add(auxBankAcc);
        balanceComp.add(auxBankAcc);
    }

    public void printAccounts(double minBalance, double maxBalance) {
        ArrayList<BankAccount> bankAccounts = new ArrayList<BankAccount>(ownerComp);
        System.out.print("All accounts between min and max range: ");
        for (int i = 0; i < bankAccounts.size(); i++)
            if ((bankAccounts.get(i).getBalance() >= minBalance) & (bankAccounts.get(i).getBalance() <= maxBalance))
                System.out.print(bankAccounts.get(i).getOwner() + " " + bankAccounts.get(i).getBalance() + "; ");
    }


}

