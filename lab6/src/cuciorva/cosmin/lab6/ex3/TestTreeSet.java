package cuciorva.cosmin.lab6.ex3;

public class TestTreeSet {
    public static void main(String[] args) {
        BankTreeSet bankTreeSet = new BankTreeSet();
        bankTreeSet.addAccount("Razvan", 224);
        bankTreeSet.addAccount("Bogdan", 1153);
        bankTreeSet.addAccount("Sergiu", 964);
        bankTreeSet.addAccount("Artene", 546);


        bankTreeSet.printAccounts();
        System.out.println("--------------------------------");
        bankTreeSet.getAllAccounts();
        System.out.println("--------------------------------");
        System.out.println(bankTreeSet.getAccount("Artene"));
        System.out.println("--------------------------------");
        bankTreeSet.printAccounts(200,1932);

    }
}
