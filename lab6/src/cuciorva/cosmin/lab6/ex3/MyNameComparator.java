package cuciorva.cosmin.lab6.ex3;

import cuciorva.cosmin.lab6.ex1.BankAccount;

import java.util.Comparator;

public class MyNameComparator implements Comparator<BankAccount> {
    @Override
    public int compare(BankAccount o1, BankAccount o2) {
        return o1.getOwner().compareTo(o2.getOwner());
    }
}
