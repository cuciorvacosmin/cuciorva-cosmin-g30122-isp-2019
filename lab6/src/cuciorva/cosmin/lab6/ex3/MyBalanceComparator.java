package cuciorva.cosmin.lab6.ex3;

import cuciorva.cosmin.lab6.ex1.BankAccount;

import java.util.Comparator;

public class MyBalanceComparator implements Comparator<BankAccount> {
    @Override
    public int compare(BankAccount o1, BankAccount o2) {
        if (o1.getBalance() > o2.getBalance())
            return 1;
        else if (o1.getBalance() == o2.getBalance())
            return 0;
        else
            return -1;
    }
}
