package cuciorva.cosmin.lab6.ex1;

public class BankAccount {
    private String owner;
    private double balance;

    public BankAccount() {
        this.owner = "OWNER";
        this.balance = 0;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getOwner() {
        return owner;
    }

    public double getBalance() {
        return balance;
    }

    public BankAccount(String owner, double balance) {
        this.owner = owner;
        this.balance = balance;
    }

    private void withdraw(double amount) {
        System.out.println("Ai retras suma de:" + amount + " sold actual:" + (this.balance - amount));
    }

    private void deposit(double amount) {
        System.out.println("Ai depus suma de:" + amount + " sold actual:" + (this.balance + amount));
    }

    public void withdrawAndDeposit(double withDraw, double deposit) {
        withdraw(withDraw);
        deposit(deposit);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof BankAccount) {
            BankAccount bankAcc = (BankAccount) obj;
            return balance == bankAcc.balance && owner == bankAcc.owner;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return (int) (owner.hashCode() + balance);
    }
}
