package cuciorva.cosmin.lab6.ex1;

public class TestBankAcc {
    public static void main(String[] args) {
        BankAccount bankAccount = new BankAccount("Admin", 321.15);
        BankAccount bankAccount1 = new BankAccount("Admin", 321.15);
        BankAccount bankAccount2 = new BankAccount("AdminSec", 3462);
        bankAccount.withdrawAndDeposit(65, 55);
        bankAccount2.withdrawAndDeposit(65, 55);

        if (!(bankAccount.equals(bankAccount1))) {
            System.out.println(bankAccount.hashCode() + " != " + bankAccount1.hashCode());
        } else {
            System.out.println(bankAccount.hashCode() + " = " + bankAccount1.hashCode());
        }
        if (bankAccount.equals(bankAccount2)) {
            System.out.println(bankAccount.hashCode() + " = " + bankAccount2.hashCode());
        } else {
            System.out.println(bankAccount.hashCode() + " != " + bankAccount2.hashCode());
        }
    }
}
