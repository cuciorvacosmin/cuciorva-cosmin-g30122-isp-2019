package cuciorva.cosmin.lab6.ex2;

import cuciorva.cosmin.lab6.ex1.BankAccount;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Bank extends BankAccount {
    ArrayList<BankAccount> bankAccounts = new ArrayList<BankAccount>();

    public Bank() {
    }

    public Bank(String owner, double balance, ArrayList<BankAccount> bankAccounts) {
        super(owner, balance);
        this.bankAccounts = bankAccounts;
    }

    public void addAccount(String owner, double balance) {
        BankAccount auxBankAcc = new BankAccount(owner, balance);
        bankAccounts.add(auxBankAcc);
    }

    public void printAccounts() {

        Collections.sort(bankAccounts, NUMBERS_ORDER);
        print();
    }

    public BankAccount getAccount(String owner) {

        for (int i = 0; i < bankAccounts.size(); i++) {
            if (bankAccounts.get(i).getOwner() == owner) {
                return bankAccounts.get(i);
            }
        }
        return null;
    }

    public void printAccounts(double minBalance, double maxBalance) {
        System.out.print("All accounts between min and max range: ");
        for (int i = 0; i < bankAccounts.size(); i++)
            if ((bankAccounts.get(i).getBalance() >= minBalance) & (bankAccounts.get(i).getBalance() <= maxBalance))
                System.out.print(bankAccounts.get(i).getOwner() + " " + bankAccounts.get(i).getBalance() + "; ");
    }

    public void print() {
        for (int i = 0; i < bankAccounts.size(); i++) {
            System.out.println(bankAccounts.get(i).getOwner() + " " + bankAccounts.get(i).getBalance());
        }
    }

    public void getAllAccounts() {
        Collections.sort(bankAccounts, ALPHABETICAL_ORDER);
        print();
    }

    protected static Comparator<BankAccount> ALPHABETICAL_ORDER = (b1, b2) -> b1.getOwner().compareTo(b2.getOwner());
    protected static Comparator<BankAccount> NUMBERS_ORDER = (o1, o2) -> Double.compare(o1.getBalance(), o2.getBalance());
}
