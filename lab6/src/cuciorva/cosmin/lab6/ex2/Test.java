package cuciorva.cosmin.lab6.ex2;

import cuciorva.cosmin.lab6.ex1.BankAccount;

public class Test {
    public static void main(String[] args) {
        Bank banca = new Bank();
        BankAccount bankAccount = new BankAccount("George", 9931);
        BankAccount bankAccount1 = new BankAccount("Radu", 5512.15);
        BankAccount bankAccount2 = new BankAccount("Cosmin", 321.15);
        BankAccount bankAccount3 = new BankAccount("Xonia", 3462);
        banca.bankAccounts.add(bankAccount);
        banca.bankAccounts.add(bankAccount1);
        banca.bankAccounts.add(bankAccount2);
        banca.bankAccounts.add(bankAccount3);

        banca.getAllAccounts();
        System.out.println("---------------------------------------");
        banca.printAccounts();
        System.out.println("---------------------------------------");
        banca.printAccounts(2000, 10000);

        banca.addAccount("Sergiu", 8800);

        System.out.println("");
        banca.getAllAccounts();
        System.out.println("---------------------------------------");
        banca.printAccounts();
        banca.printAccounts(2000, 10000);
        System.out.println("");
        System.out.println("---------------------------------------");
        //System.out.println(banca.getAccount("Cosmin").getOwner() + " " + banca.getAccount("Cosmin").getBalance());

    }
}
