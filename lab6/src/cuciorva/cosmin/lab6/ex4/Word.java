package cuciorva.cosmin.lab6.ex4;

public class Word {

    private String name;

    public Word() {
    }

    public Word(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
