package cuciorva.cosmin.lab6.ex4;

public class Test {


    public static void main(String[] args) {
        Dictionary dict = new Dictionary();

        Word word = new Word("Caine");
        Word word1 = new Word("Pisica");
        Word word2 = new Word("Linghișpir");
        Definition definition = new Definition("Mamifer carnivor din familia canidelor, domesticit, folosit pentru pază, vânătoare etc. (Canis familiaris)");
        Definition definition1 = new Definition("Mamifer domestic carnivor din familia felidelor, cu corpul suplu, acoperit cu blană deasă și moale de diferite culori, cu capul rotund, cu botul foarte scurt, cu maxilarele puternice");
        Definition definition2 = new Definition("Instalație cu o pistă rotativă, pe care sunt montate jucării (mașinuțe, căluți de lemn etc.), specifice parcurilor de distracții; carusel. ");
        dict.addWord(word, definition);
        dict.addWord(word1, definition1);
        dict.addWord(word2, definition2);

        System.out.println(dict.dictionary.get(word).getDescription());
        System.out.println("--------------------------------------------------------------------------------");
        System.out.println(dict.getDefinition(word1).getDescription());
        System.out.println("--------------------------------------------------------------------------------");
        System.out.println(dict.dictionary.get(word2).getDescription());
        System.out.println("--------------------------------------------------------------------------------");
        dict.getAllWords();
        System.out.println("--------------------------------------------------------------------------------");
        dict.getAllDescription();


    }

}



