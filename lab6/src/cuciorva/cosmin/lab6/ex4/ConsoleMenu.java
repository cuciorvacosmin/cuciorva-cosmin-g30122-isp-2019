package cuciorva.cosmin.lab6.ex4;

import java.util.Scanner;

public class ConsoleMenu {

    public static void main(String[] args) {
        int optiune = 0;
        Scanner scanner = new Scanner(System.in);
        Dictionary dict = new Dictionary();
        while (optiune != 6) {
            System.out.println("\n 1.AddWord \n 2.GetDefinition \n 3.GetAllWords \n 4.GetAllDescription \n 5.Exit \n Optiune dorita: ");
            optiune = scanner.nextInt();
            switch (optiune) {
                case 1:
                    dict.readWordDefinition();
                    break;
                case 2:
                    dict.readW();
                    break;
                case 3:
                    dict.getAllWords();
                    break;
                case 4:
                    dict.getAllDescription();
                    break;
                case 5:
                    return;
                default:
                    System.out.println("Optiune inexistenta");
            }
        }
    }
}
