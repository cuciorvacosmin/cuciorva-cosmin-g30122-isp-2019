package cuciorva.cosmin.lab6.ex4;

import java.util.HashMap;
import java.util.Scanner;

public class Dictionary {

    HashMap<Word, Definition> dictionary = new HashMap<Word, Definition>();

    public void addWord(Word word, Definition definition) {
        dictionary.put(word, definition);
    }

    public Definition getDefinition(Word w) {
        Definition aux = new Definition("");
        if (dictionary.containsKey(w)) {
            aux.setDescription(dictionary.get(w).getDescription());
        }
        return aux;
    }

    public void getAllWords() {
        for (Word word : dictionary.keySet()) {
            System.out.println(word.getName());
        }
    }

    public void getAllDescription() {
        for (Definition definition : dictionary.values()) {
            System.out.println(definition.getDescription());
        }
    }

    public void readWordDefinition() {
        String cuvant;
        String descriere;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Cuvantul pe care doriti sa-l adaugati este:");
        cuvant = scanner.nextLine();
        System.out.printf("User input was: %s%n", cuvant);
        System.out.println("Descrieti cuvant pe care l-ai adaugat:");
        descriere = scanner.next();
        Word word = new Word(cuvant);
        Definition definition = new Definition(descriere);
        addWord(word, definition);
    }

    public void readW() {
        boolean value = false;
        String cuvant;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Cuvantul a carei definitie o doriti:");
        cuvant = scanner.nextLine();
        Word w = new Word(cuvant);
        Definition aux = new Definition("");
        for (Word word : dictionary.keySet()) {

            if (word.getName().equals(w.getName())) {
                System.out.println("Definitia cuvantul este: \n" + dictionary.get(word).getDescription());
                value = true;
            }
            if (!value) {
                System.out.println("Cuvantul introdus nu exista!");
            }
        }
    }
}






