package cuciorva.cosmin.lab9.ex3;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;

public class EditorAndTextField extends JFrame {

    JButton button = new JButton("Get text file");
    JTextField textField = new JTextField("Enter file name here");
    final JTextArea ta = new JTextArea(30, 60);

    EditorAndTextField() {
        setTitle("Exercitiul 3");
        getContentPane().add(new JScrollPane(ta), BorderLayout.NORTH);
        getContentPane().add(button, BorderLayout.WEST);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        pack();
        setSize(400, 400);
        setVisible(true);
    }

    public void init() {
        this.setLayout(null);
        button.setBounds(10, 75, 100, 20);
        button.addActionListener(new A());

        textField.setBounds(10, 25, 120, 20);


        ta.setBounds(10, 100, 200, 200);


        add(ta);
        add(button);
        add(textField);

    }

    class A implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            try {


                File f = new File("C:\\Users\\cucio\\IdeaProjects\\cuciorva-cosmin-g30122-isp-2019");
                File[] matchingFiles = f.listFiles(new FilenameFilter() {
                    public boolean accept(File dir, String name) {
                        return name.startsWith(textField.getText()) && name.endsWith("txt");
                    }
                });
                FileReader reader = new FileReader(textField.getText());
                BufferedReader br = new BufferedReader(reader);
                ta.read(br, null);
                br.close();
                ta.requestFocus();
                textField.setText("");

            } catch (Exception e2) {
                System.out.println(e2);
            }
        }
    }
    public static void main(String[] args) {
        EditorAndTextField a = new EditorAndTextField();
    }
}
