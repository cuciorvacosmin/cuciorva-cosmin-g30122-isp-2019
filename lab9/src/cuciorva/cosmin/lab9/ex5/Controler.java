package cuciorva.cosmin.lab9.ex5;

import java.util.ArrayList;
import java.util.List;

public class Controler {
    String stationName;
    List<Controler> neighbourControllers = new ArrayList<Controler>();
    //storing station train track segments
    private ArrayList<Segment> list = new ArrayList<Segment>();

    Controler(String gara) {
        stationName = gara;
    }

    void setNeighbourController(Controler v) {
        neighbourControllers.add(v);
    }

    void addControlledSegment(Segment s) {
        list.add(s);
    }

    /**
     * Check controlled segments and return the id of the first free segment or -1 in case there is no free segment in this station
     *
     * @return segment id if there is a free one
     */
    private int getFreeSegmentId() {
        for (Segment segment : list) {
            if (!segment.hasTrain())
                return segment.id;
        }
        return -1;
    }

    public void controlStep() {
        //check which train must be sent

        for (Segment segment : list) {
            if (segment.hasTrain()) {
                Train train = segment.getTrain();
                for (Controler neighbourController : neighbourControllers) {
                    if (train.getDestination().equals(neighbourController.stationName)) {
                        //check if there is a free segment
                        int id = neighbourController.getFreeSegmentId();
                        if (id == -1) {
                            TrainApp.systemOut("Trenul +" + train.name + "din gara " + stationName + " nu poate fi trimis catre " + neighbourController.stationName + ". Nici un segment disponibil!");
                            return;
                        }
                        //send train
                        TrainApp.systemOut("Trenul " + train.name + " pleaca din gara " + stationName + " spre gara " + neighbourController.stationName);
                        segment.departTRain();
                        neighbourController.arriveTrain(train, id);
                    }
                }//.for

            }
        }//.for

    }//.


    public void arriveTrain(Train train, int idSegment) {
        for (Segment segment : list) {
            //search id segment and add train on it
            if (segment.id == idSegment)
                if (segment.hasTrain()) {
                    TrainApp.systemOut("CRASH! Train " + train.name + " colided with " + segment.getTrain().name + " on segment " + segment.id + " in station " + stationName);
                    return;
                } else {
                    TrainApp.systemOut("Train " + train.name + " arrived on segment " + segment.id + " in station " + stationName);
                    segment.arriveTrain(train);
                    return;
                }
        }

        //this should not happen
        TrainApp.systemOut("Train " + train.name + " cannot be received " + stationName + ". Check controller logic algorithm!");

    }


    public void displayStationState() {
        TrainApp.systemOut("=== STATION " + stationName + " ===");
        for (Segment segment : list) {
            if (segment.hasTrain())
                TrainApp.systemOut("|----------ID=" + segment.id + "__Train=" + segment.getTrain().name + " to " + segment.getTrain().destination + "__----------|");
            else
                TrainApp.systemOut("|----------ID=" + segment.id + "__Train=______ to ________----------|");
        }
    }

    public static void main(String[] args) {
        new TrainApp();
    }
}
