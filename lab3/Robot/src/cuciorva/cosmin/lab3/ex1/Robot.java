package cuciorva.cosmin.lab3.ex1;

public class Robot {
    private int x;

    public Robot() {
        this.x = 1;
    }

    public void change(int k) {
        if (k >= 1) {
            x += k;
        } else {
            System.out.println("Method stopped!");
        }
    }
    @Override
    public String toString() {
        return "The position of robot is " + this.x;
    }
}
