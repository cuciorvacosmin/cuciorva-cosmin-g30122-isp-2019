package cuciorva.cosmin.lab3.ex1;

public class Sensor {
    int value;

    Sensor() {
        this.value = -1;
    }

    public void change(int k) {
        this.value = k;
    }

    public void toString1() {

        System.out.println("Sensor value is :"+this.value);
    }
}
