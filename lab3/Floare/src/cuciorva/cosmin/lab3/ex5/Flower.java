package cuciorva.cosmin.lab3.ex5;

public class Flower {
    private int petal;
    private static int numberObj = 0;

    Flower() {
        System.out.println("Flower has been created!");
        numberObj++;
    }

    public static void main(String[] args) {
        Flower[] garden = new Flower[5];
        for (int i = 0; i < 5; i++) {
            Flower f = new Flower();
            garden[i] = f;              //nr de obiecte create e garden.length()
            if (i == garden.length - 1) {
                System.out.println("Number of created objects is: " + numberObj);
            }
        }
    }
}

