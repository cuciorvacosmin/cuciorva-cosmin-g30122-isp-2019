package cuciorva.cosmin.lab3.ex4;

public class TestMyPoint {
    public static void main(String[] args) {
        MyPoint p1 = new MyPoint();
        MyPoint p2 = new MyPoint(3, 4);

        //System.out.println(p1.getX());    tested
        //System.out.println(p2.getX());    tested
        //System.out.println(p1.getY());    tested
        //System.out.println(p2.getY());    tested
        System.out.print("p1 with  coordonates:");
        p1.toString();  //better
        System.out.print("p2 with coordonates:");
        p2.toString();  //better

        System.out.print("distance(using first method)= ");
        System.out.println(p1.distance(3, 4));

        System.out.print("distance(using second method)=");
        System.out.println(p1.distance(p2));

        p1.setXY(5, 5);
        System.out.print("p1 with new coordonates:");
        p1.toString();

        System.out.print("New distance: ");
        System.out.println(p1.distance(p2));

    }
}
