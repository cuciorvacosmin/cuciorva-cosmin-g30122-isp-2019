package cuciorva.cosmin.lab3.ex2;

public class TestCircle {
    public static void main(String[] args) {
        Circle c1 = new Circle();
        Circle c2 = new Circle(4);
        Circle c3 = new Circle(2, "blue");
        c1.getRadius();
        c2.getRadius();
        c3.getRadius();

        c1.getColor();
        c2.getColor();
        c3.getColor();

        c1.getArea();
        c2.getArea();
        c3.getArea();

    }
}
