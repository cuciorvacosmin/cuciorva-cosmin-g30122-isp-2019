package cuciorva.cosmin.lab3.ex2;

public class Circle {
    double radius;
    String color;

    public Circle() {
        this.radius = 1.0;
        this.color = "red";
    }

    public Circle(double radius,String color) {
        this.radius=radius;
        this.color = color;
    }

    public Circle(double radius) {
        this.radius = radius;
    }
    public void getRadius(){
        System.out.println("Radius= "+radius);
    }
    public void getArea() {
        double Area;
        Area=Math.PI*(this.radius*this.radius);
        System.out.println("Area of this circle is: "+Area);
    }
    public void getColor(){
        System.out.println("Color is "+this.color);
    }
}
