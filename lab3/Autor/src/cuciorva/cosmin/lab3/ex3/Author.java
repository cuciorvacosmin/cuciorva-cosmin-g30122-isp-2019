package cuciorva.cosmin.lab3.ex3;

public class Author {
    String name, email, gender;

    public Author(String name, String email, String gender) {
        this.name = name;
        this.email = email;
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getGender() {
        return gender;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        System.out.println(this.name + "(" + this.gender + ") at " + this.email);
        return null;
    }
}
