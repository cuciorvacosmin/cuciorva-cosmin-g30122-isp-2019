package cuciorva.cosmin.lab11.ex1;

import java.util.Observable;

interface Observer {
    void update (Observable t, Object o);
}

