package cuciorva.cosmin.lab8.ex3;

import java.util.ArrayList;

class Controler {

    String stationName;
    Controler neighbourController;
    ArrayList<Controler> neighbourControl = new ArrayList<>();


    //storing station train track segments
    ArrayList<Segment> list = new ArrayList<Segment>();

    public Controler(String gara) {
        stationName = gara;
    }

    void setNeighbourController(ArrayList<Controler> v) {
        neighbourControl = v;
        setListValid();
        System.out.println("(" + this.getStationName() + ");" + "Puteti lua legatura cu:");
        int i = 0;
        for (Controler c : neighbourControl) {
            ++i;
            System.out.println(i + "." + c.getStationName());
        }
        System.out.println("");
        neighbourControl.add(this);
    }

    void setListValid() {
        ArrayList<Controler> toRemove = new ArrayList<>();

        for (Controler c : neighbourControl) {
            if (c.getStationName().equals(this.getStationName())) {
                toRemove.add(this);
            }
        }
        neighbourControl.removeAll(toRemove);
    }

    public String getStationName() {
        return stationName;
    }

    void addControlledSegment(Segment s) {
        list.add(s);
    }

    /**
     * Check controlled segments and return the id of the first free segment or -1 in case there is no free segment in this station
     *
     * @return
     */
    int getFreeSegmentId() {
        for (Segment s : list) {
            if (s.hasTrain() == false)
                return s.id;
        }
        return -1;
    }

    void controlStep(ArrayList<Controler> v) {
        //check which train must be sent
        neighbourControl = v;
        for (Segment segment : list) {
            if (segment.hasTrain()) {
                Train t = segment.getTrain();
                setListValid();
                for (Controler c : neighbourControl) {
                    if (t.getDestination().equals(c.getStationName())) {
                        //check if there is a free segment
                        int id = c.getFreeSegmentId();
                        if (id == -1) {
                            System.out.println("Trenul +" + t.name + "din gara " + stationName + " nu poate fi trimis catre " + c.getStationName() + ". Nici un segment disponibil!");
                            return;
                        }
                        //send train
                        System.out.println("Trenul " + t.name + " pleaca din gara " + stationName + " spre gara " + c.getStationName());
                        segment.departTRain();
                        c.arriveTrain(t, id);
                    }
                }

            }
        }//.for
        neighbourControl.add(this);
    }//.


    public void arriveTrain(Train t, int idSegment) {
        for (Segment segment : list) {
            //search id segment and add train on it
            if (segment.id == idSegment)
                if (segment.hasTrain() == true) {
                    System.out.println("CRASH! Train " + t.name + " colided with " + segment.getTrain().name + " on segment " + segment.id + " in station " + stationName);
                    return;
                } else {
                    System.out.println("Train " + t.name + " arrived on segment " + segment.id + " in station " + stationName);
                    segment.arriveTrain(t);
                    return;
                }
        }

        //this should not happen
        System.out.println("Train " + t.name + " cannot be received " + stationName + ". Check controller logic algorithm!");

    }


    public void displayStationState() {
        System.out.println("=== STATION " + stationName + " ===");
        for (Segment s : list) {
            if (s.hasTrain())
                System.out.println("|----------ID=" + s.id + "__Train=" + s.getTrain().name + " to " + s.getTrain().destination + "__----------|");
            else
                System.out.println("|----------ID=" + s.id + "__Train=______ catre ________----------|");
        }
    }
}
