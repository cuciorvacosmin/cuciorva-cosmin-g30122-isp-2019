package cuciorva.cosmin.lab8.ex4;

public class GsmUnit extends Unit {

    public GsmUnit() {
        super(UnitType.GSM);
    }

    void callOwner(){
        System.out.println("The owner has been called!");
    }

    @Override
    public String toString() {
        return "The owner has been called";
    }
}
