package cuciorva.cosmin.lab8.ex4;

public class HomeAutomation {
    private HomeAutomation() {
    }

    private static String messageFinal;

    public static void main(String[] args) {

        //test using an annonimous inner class



        Home h = new Home() {
            protected void setValueInEnvironment(Event event) {
                String message = "\n"+"New event in environment " + event;
                System.out.println(message);
                messageFinal += message;

            }

            protected void controllStep() {
                String message = "Control step executed" + "\n";
                System.out.println(message);
                messageFinal += message;
                writeFile(messageFinal);
            }

            protected void setUnit(Unit u) {
                messageFinal = "";
                String message = "New unit in environment " + u;
                System.out.println(message);
                messageFinal += message;
            }

        };
        h.simulate();

       // h.writeFile(messageFinal);
    }
}

