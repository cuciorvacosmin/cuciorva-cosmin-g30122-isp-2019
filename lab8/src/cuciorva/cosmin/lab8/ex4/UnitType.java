package cuciorva.cosmin.lab8.ex4;

public enum UnitType {
    ALARM, HEATING, COOLING, GSM;
}
