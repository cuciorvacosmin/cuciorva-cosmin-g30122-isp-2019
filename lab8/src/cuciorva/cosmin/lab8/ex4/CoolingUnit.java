package cuciorva.cosmin.lab8.ex4;

public class CoolingUnit extends Unit {
    public CoolingUnit() {
        super(UnitType.COOLING);
    }

    void startCooling(){
        System.out.println("Cooling unit ON");
    }

    @Override
    public String toString() {
        return "Cooling unit ON";
    }
}
