package cuciorva.cosmin.lab8.ex4;

public class ControlUnit {

    private static ControlUnit unit;
    TemperatureSensor temperatureSensor;
    GsmUnit gsmUnit;
    CoolingUnit coolingUnit;
    HeatingUnit heatingUnit;
    AlarmUnit alarmUnit;
    FireSensor fireSensor;

    void controEvent(Event event){
        if(event instanceof TemperatureEvent){
            if(temperatureSensor.checkTemperature((TemperatureEvent)event)==1){
                coolingUnit.startCooling();
            }else if(temperatureSensor.checkTemperature((TemperatureEvent)event)==-1){
                heatingUnit.startHeating();
            }else {
                System.out.println("Perfect!");
            }
        }else if(event instanceof FireEvent){
            if(fireSensor.checkSmoke((FireEvent)event)){
                alarmUnit.startAlarm();
                gsmUnit.callOwner();
            }else {
                System.out.println("Nothing to be afraid of!");
            }
        }
    }

    private ControlUnit() {
    }

    public static ControlUnit getInstance() {

        if (unit == null) {
            unit = new ControlUnit();
        }
        return unit;
    }
}
