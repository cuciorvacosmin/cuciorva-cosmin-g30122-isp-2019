package cuciorva.cosmin.lab8.ex4;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

public abstract class Home {

    private Random r = new Random();
    private final int SIMULATION_STEPS = 20;
    PrintWriter writer = null;

    public Home() {
        // TODO Auto-generated constructor stub
    }

    protected abstract void setValueInEnvironment(Event event);

    protected abstract void controllStep();

    protected abstract void setUnit(Unit u);

    private Event getHomeEvent() {
        //randomly generate a new event;
        int k = r.nextInt(100);
        if (k < 30) {
               //writer.println(new NoEvent());
            return new NoEvent();
        } else if (k < 60) {
            FireEvent f = new FireEvent(r.nextBoolean());
            if (f.isSmoke() == true) {
                setUnit(new AlarmUnit());
                setUnit(new GsmUnit());
               // writer.println(new AlarmUnit());
                //writer.println(new GsmUnit());
            }
            return f;
        } else {
            TemperatureEvent t = new TemperatureEvent(r.nextInt(50));
            if (t.getVlaue() < 23) {
                setUnit(new HeatingUnit());
//                writer.println(new HeatingUnit());
            } else {
                setUnit(new CoolingUnit());
                //  writer.println(new CoolingUnit());
            }
            return t;
        }
    }

    public void simulate() {
        int k = 0;
        while (k < SIMULATION_STEPS) {
            Event event = this.getHomeEvent();
            setValueInEnvironment(event);
            controllStep();
            try {
                Thread.sleep(300);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }

            k++;
        }
    }
    public static void writeFile(String message){
        try{
            PrintWriter writer = new PrintWriter("system_logs.txt" );
            writer.println(message);
            writer.close();
            System.out.println(message);
        }
        catch (IOException x){
            System.err.println(x);
        }
    }

}