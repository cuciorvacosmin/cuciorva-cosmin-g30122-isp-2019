package cuciorva.cosmin.lab8.ex4;

public class AlarmUnit extends Unit {


    public AlarmUnit() {
        super(UnitType.ALARM);
    }

    void startAlarm(){
        System.out.println("FIRE Alarm ON");
    }

    @Override
    public String toString() {
        return "FIRE Alarm ON";
    }
}
