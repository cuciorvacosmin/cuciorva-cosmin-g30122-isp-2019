package cuciorva.cosmin.lab8.ex4;

public class FireSensor extends FireEvent {

    FireSensor(boolean smoke) {
        super(smoke);
    }

    boolean checkSmoke(FireEvent fireEvent){
        return isSmoke();
    }

    @Override
    public String toString() {
        if (isSmoke()) {
            return "\n" + new AlarmUnit() + new GsmUnit();
        } else {
            return "\n" + "No Action,everything is alright";
        }
    }
}
