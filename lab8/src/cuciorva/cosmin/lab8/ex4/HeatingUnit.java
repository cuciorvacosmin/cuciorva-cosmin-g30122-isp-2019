package cuciorva.cosmin.lab8.ex4;

public class HeatingUnit extends Unit{


    public HeatingUnit() {
        super(UnitType.ALARM.HEATING);
    }
    void startHeating(){
        System.out.println("Heating unit ON");
    }

    @Override
    public String toString() {
        return "Heating unit ON";
    }
}
