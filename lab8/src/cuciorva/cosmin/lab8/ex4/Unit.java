package cuciorva.cosmin.lab8.ex4;

abstract class Unit {
    UnitType type;

    Unit(UnitType type){
        this.type = type;
    }

    UnitType getType(){
        return type;
    }

}
