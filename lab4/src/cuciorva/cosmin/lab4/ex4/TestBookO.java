package cuciorva.cosmin.lab4.ex4;

import cuciorva.cosmin.lab4.ex2.Author;

import java.util.Scanner;

public class TestBookO {
    public static void main(String[] args) {
        Author[] author = new Author[3];
        author[0] = new Author("Popescu");
        author[1] = new Author("Pop");
        author[2] = new Author("Dragnea");

        BookO carte = new BookO("Fifty shades of Grey", author, 129.69);

        carte.printAuthors();
        System.out.println(carte.toString());
    }
}
