package cuciorva.cosmin.lab4.ex4;

import cuciorva.cosmin.lab4.ex2.Author;

public class BookO {
    private String name;
    private Author[] author;
    private double price;
    private int qtyInStock = 0;

    public BookO() {
    }

    public BookO(String name, Author[] author, double price) {
        this.name = name;
        this.author = author;
        this.price = price;
    }

    public BookO(String name, Author[] author, double price, int qtyInStock) {
        this.name = name;
        this.author = author;
        this.price = price;
        this.qtyInStock = qtyInStock;
    }

    public String getName() {
        return name;
    }

    public Author[] getAuthor() {
        return author;
    }

    public double getPrice() {
        return price;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }

    @Override
    public String toString() {
        return this.name + " by " + this.author.length + " different authors";
    }

    public void printAuthors() {
        for (int i = 0; i < this.author.length; i++)
            System.out.println("Authors names: " + author[i].getName());
    }
}
