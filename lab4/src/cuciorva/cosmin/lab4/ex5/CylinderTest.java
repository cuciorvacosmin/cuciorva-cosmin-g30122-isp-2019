package cuciorva.cosmin.lab4.ex5;

import cuciorva.cosmin.lab4.ex1.Circle;

public class CylinderTest {
    public static void main(String[] args) {
        Cylinder cilindru = new Cylinder(2.2, 5);
        Circle cerc = new Circle(2.2);

        System.out.println("Cylinder Area " + cilindru.getArea());
        System.out.println("Circle Area " + cerc.getArea());
        System.out.println("Overwrited succesfully!:)");

        System.out.println("Cylinder Volume " + cilindru.getVolume());
        System.out.println("Radius " + cilindru.getRadius());
    }
}
