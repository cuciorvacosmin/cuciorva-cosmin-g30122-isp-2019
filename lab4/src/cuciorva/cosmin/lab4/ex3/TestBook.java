package cuciorva.cosmin.lab4.ex3;

import cuciorva.cosmin.lab4.ex2.Author;

public class TestBook {
    public static void main(String[] args) {
        Author author1 = new Author("Radu", "radupuiu@gmail.com", 'm');
        Author author2 = new Author("Alexandra", "bradatanualexandra@gmail.com", 'f');

        Book carte1 = new Book("1001 nights", author1, 89.98, 43);
        Book carte2 = new Book("Game of Thrones vol.1", author2, 149.99);


        System.out.println("Name :" + carte1.getName());
        System.out.println("Author :" + carte1.getAuthor().getName());
        System.out.println("Price :" + carte1.getPrice());

        System.out.println("Price of " + carte2.getName() + " was:" + carte2.getPrice());
        carte2.setPrice(99.99);
        carte2.setQtyInStock(10);
        System.out.println("-33%, now the price is:" + carte2.getPrice() + " last " + carte2.getQtyInStock() + " books!Hurry up!");
        System.out.print("Method from Author ->");
        carte2.getAuthor().toString();
    }
}
