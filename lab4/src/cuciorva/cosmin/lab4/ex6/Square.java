package cuciorva.cosmin.lab4.ex6;

public class Square extends Rectangle {
    public Square() {
        this.setWidth(this.getLength());
    }

    public Square(double side) {
        super(side, side);
    }

    public Square(String color, boolean filled, double side) {
        super(color, filled, side, side);
    }

    public double getSide(){
        return this.getLength();
    }
    public void setSide(double side){
        this.setLength(side);
    }

    @Override
    public String toString() {
        return "A Square with side="+this.getLength()+",which is a subclass of "+super.toString();
    }

    @Override
    public void setWidth(double side) {
        this.setLength(side);
    }

    @Override
    public void setLength(double side) {
        this.setWidth(side);
    }
}
