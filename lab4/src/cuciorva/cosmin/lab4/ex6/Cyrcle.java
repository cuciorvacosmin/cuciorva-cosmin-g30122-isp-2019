package cuciorva.cosmin.lab4.ex6;

public class Cyrcle extends Shape {
    double radius = 1.0;

    public Cyrcle() {
        this.radius = 1.0;
    }

    public Cyrcle(double radius) {
        this.radius = radius;
    }

    public Cyrcle(String color, boolean filled, double radius) {
        super(color, filled);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea() {
        return Math.PI * (this.radius * this.radius);
    }

    public double getPerimeter() {
        return 2 * Math.PI * this.radius;
    }

    @Override
    public String toString() {
        return "A Circle with radius="+this.radius+",which is a subclass of "+super.toString();
    }
}

