package cuciorva.cosmin.lab4.ex6;

public class Test {
    public static void main(String[] args) {
        Cyrcle c1 = new Cyrcle(2);
        System.out.println(c1.toString());

        Square p1 = new Square("black", true, 5);
        System.out.println("Side=" + p1.getSide());
        System.out.println("Square area=" + p1.getArea());               // metoda mostenita din clasa rectangle
        System.out.println("Perimeter=" + p1.getPerimeter());         // metoda mostenita din clasa rectangle
        System.out.println(p1.toString());

        Rectangle r1 = new Rectangle("yellow", true, 4, 5);
        System.out.println(r1.toString());

        Shape s1=new Shape("white",false);
        System.out.println(s1.toString());
    }
}
