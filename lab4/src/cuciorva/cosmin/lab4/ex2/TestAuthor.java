package cuciorva.cosmin.lab4.ex2;

public class TestAuthor {
    public static void main(String[] args) {
        Author author = new Author("Cuciorva Cosmin", "cuciorvacosmin@gmail.ro", 'm');
        System.out.println(author.toString());
        author.setEmail("cosmin_cuciorva@outlook.com");
        System.out.println(author.toString());

        System.out.println("Name="+author.getName()+",gendre="+author.getGender()+",mail="+author.getEmail());
    }
}
