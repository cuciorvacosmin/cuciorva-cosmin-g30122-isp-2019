package cuciorva.cosmin.lab4.ex2;

public class Author {
    private String name, email;
    char gender;

    public Author(String name) {
        this.name = name;
    }

    public Author(String name, String email, char gender) {
        this.name = name;
        this.email = email;
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public char getGender() {
        return gender;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return this.name + "(" + this.gender + ") at " + this.email;

    }
}
