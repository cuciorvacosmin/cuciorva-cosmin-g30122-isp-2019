package cuciorva.cosmin.lab4.ex1;

public class Circle {
    double radius = 1.0;
    String color = "red";

    public Circle() {
        System.out.println("Circle constructed!");
    }
    public Circle(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return this.radius;
    }
    public double getArea() {
        return Math.PI * (this.radius * this.radius);
    }
}


