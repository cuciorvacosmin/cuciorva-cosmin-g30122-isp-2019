package cuciorva.cosmin.lab4.ex1;

public class TestCircle{
    public static void main(String[] args) {
        Circle cerc=new Circle();
        Circle cerc1=new Circle(2.2);

        System.out.println("Radius="+cerc.getRadius());
        System.out.println("Area="+cerc.getArea());

        System.out.println("Radius="+cerc1.getRadius());
        System.out.println("Area="+cerc1.getArea());
    }
}