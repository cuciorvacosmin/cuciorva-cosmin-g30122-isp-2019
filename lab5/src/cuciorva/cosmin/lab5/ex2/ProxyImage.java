package cuciorva.cosmin.lab5.ex2;

public class ProxyImage implements Image {
    Image i;
    private String fileName;

    public ProxyImage(String fileName) {
        this.fileName = fileName;
        this.display();
    }


    @Override
    public void display() {
        if (fileName == "real") {
            i = new RealImage(fileName);
            i.display();
        } else if (fileName == "rotated") {
            i= new RotatedImage(fileName);
            i.display();
        }
    }

}
