package cuciorva.cosmin.lab5.ex3;

import java.util.Timer;
import java.util.TimerTask;

public class Controller {
    TemperatureSensor tempSensor = new TemperatureSensor();
    LightSensor lightSensor = new LightSensor();
    private int count = 0;
    private static volatile Controller myController = null;

    private Controller() {
        System.out.println("Singleton successfully created!");
    }    //trebuie declarat privat; eroare la test pentru ca nu poate accesa constructorul privat

    Timer timer = new Timer();
    TimerTask task = new TimerTask() {
        @Override
        public void run() {
            count++;
            if (count <= 20)
                System.out.println(count + ".Temperature value is " + tempSensor.readValue() + " and light value is " + lightSensor.readValue());
            else {
                task.cancel();
                timer.cancel();
            }
        }
    };

    public void control() {
        timer.schedule(task, 0, 1000);
    }

    public static Controller getInstance() {
        synchronized (Controller.class) {
            if (myController == null) {
                myController = new Controller();
            }
        }
        return myController;
    }
}
