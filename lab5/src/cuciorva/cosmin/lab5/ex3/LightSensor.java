package cuciorva.cosmin.lab5.ex3;

import java.util.Random;

public class LightSensor extends Sensor {

    public int readValue() {
        Random rand = new Random();
        int randomNum = rand.nextInt(100) + 1;
        return randomNum;
    }
}
