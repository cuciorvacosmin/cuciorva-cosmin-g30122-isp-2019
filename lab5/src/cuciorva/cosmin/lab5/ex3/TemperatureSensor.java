package cuciorva.cosmin.lab5.ex3;

public class TemperatureSensor extends Sensor {


    public int readValue() {
        double randomNum = Math.random() * 100 + 1;
        return (int) randomNum;
    }
}
