package cuciorva.cosmin.lab5.ex1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws Exception {
        String nameObj;
        Scanner scanner = new Scanner(System.in);
        Shape[] shapes = new Shape[3];
        for (int i = 0; i < shapes.length; i++) {
            nameObj = scanner.nextLine();
            switch (nameObj) {
                case "Circle":
                    shapes[i] = new Circle("red", true, 2.2);
                    break;
                case "Rectangle":
                    shapes[i] = new Rectangle("red", false, 2, 2);
                    break;
                case "Square":
                    shapes[i] = new Square("yellow", false, 4);
                    break;
                default:
                    throw new Exception("Invalid choise!");
            }
        }
        for (int i = 0; i < shapes.length; i++) {
            System.out.println(shapes[i].toString()+" with Area= "+shapes[i].getArea()+" and perimeter= "+shapes[i].getPerimeter());
        }
    }
}
