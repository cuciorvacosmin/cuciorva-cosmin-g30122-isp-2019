package cuciorva.cosmin.lab5.ex1;

abstract class Shape {
    protected String color;
    protected boolean filled;

    public Shape() {
        System.out.println("Shape created!(default constructor)");
    }

    public Shape(String color, boolean filled) {
        this.color = color;
        this.filled = filled;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isFilled() {
        return filled;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    abstract double getArea();

    abstract double getPerimeter();

    @Override
    public String toString() {
        return "Shape{} color="+this.getColor()+" filled="+this.isFilled();
    }
}
