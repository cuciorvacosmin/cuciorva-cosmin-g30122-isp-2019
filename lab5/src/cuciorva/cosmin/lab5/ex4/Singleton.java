package cuciorva.cosmin.lab5.ex4;

import cuciorva.cosmin.lab5.ex3.Controller;

public class Singleton {
    public static void main(String[] args) {
        Controller myController = Controller.getInstance();
        Controller myController2=Controller.getInstance();
        System.out.println(myController.getInstance());
        System.out.println(myController2.getInstance());
    }
}
