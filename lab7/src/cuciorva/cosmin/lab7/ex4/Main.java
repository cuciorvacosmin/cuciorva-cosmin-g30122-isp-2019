package cuciorva.cosmin.lab7.ex4;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Car car1 = new Car("BMW", 12000);
        Car car2 = new Car("WW", 5800);
        Car car3 = new Car("Audi", 8900);
        Scanner scanner=new Scanner(System.in);
        int op=-1;

        while(op>4){
            System.out.println("1.Adauga element\n2.Serializare\n3.Deserializare\n4.Exit\nIntroduceti optiune dorita:");
            op=scanner.nextInt();
            switch (op){
                case 1: App.addCar();break;
                case 2:App.serializing();break;
                case 3:App.deserializing();break;
                case 4: return;
                default:
                    System.out.println("Enter a valid option!");
            }
        }
//        App.serializing();
//        App.deserializing();
    }
}
