package cuciorva.cosmin.lab7.ex4;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class App {
    public static ArrayList<Car> cars = new ArrayList<>();
    public static void serializing() {

        try {
            FileOutputStream fileOut =
                    new FileOutputStream("car.ser");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(cars);
            out.close();
            fileOut.close();
            System.out.println("Serialized data is saved in car.ser");
        } catch (IOException i) {
            i.printStackTrace();
        }
    }

    public static void deserializing() {
        try {
            FileInputStream fileIn = new FileInputStream("car.ser");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            cars = (ArrayList<Car>) in.readObject();
            for (int i = 0; i < cars.size(); i++) {
                System.out.println(cars.get(i).getPrice() + " " + cars.get(i).getModel());
            }
            in.close();
            fileIn.close();
        } catch (IOException i) {
            System.out.println("IO error");
            return;
        } catch (ClassNotFoundException c) {
            System.out.println("Car class not found");
            c.printStackTrace();
            return;
        }
    }

    public static void addCar(){
        Scanner scanner=new Scanner(System.in);
        System.out.println("Modelul masiniieste:");
        String model=scanner.nextLine();
        System.out.println("Pretul masinii este:");
        double price=scanner.nextDouble();
        Car car=new Car(model,price);
        cars.add(car);
    }
}
