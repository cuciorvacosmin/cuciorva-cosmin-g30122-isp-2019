package cuciorva.cosmin.lab7.ex2;

import java.util.Scanner;

public class Tet {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Caracterul pe care il cautati in fisier este:");
        char c = scanner.nextLine().charAt(0);
        ParticularCharacter particularCharacter = new ParticularCharacter(c);
        particularCharacter.appereance();
    }
}
