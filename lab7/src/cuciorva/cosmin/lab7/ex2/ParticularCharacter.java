package cuciorva.cosmin.lab7.ex2;

import java.io.*;

public class ParticularCharacter {
    BufferedReader br = null;
    private char ch;
    public static int contorCh = 0;

    public ParticularCharacter(char ch) {
        this.ch = ch;
    }

    public char getCh() {
        return ch;
    }

    public void appereance() {
        try {
            br = new BufferedReader(new FileReader(new File("data.txt")));
            String line = br.readLine();
            while (line != null) {
                String[] words = line.split(" ");
                int index;
                for (String word : words) {
                    index = word.indexOf(ch);
                    if (index != -1) {
                        while (index > -1) {
                            contorCh++;
                            index = word.indexOf(ch, index + 1);
                        }
                    }
                }
                line = br.readLine();
            }
            System.out.println("Caracterul a fost de gasit in fisier de:" + contorCh);
        } catch (FileNotFoundException e) {
            System.out.println("Fisierul nu s-a putut deschide!");
        } catch (IOException e) {
            System.out.println("Nu s-a putut citi linia!");
        } finally {
            try {
                br.close();           //Closing the reader
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}

