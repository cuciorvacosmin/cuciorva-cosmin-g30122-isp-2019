package cuciorva.cosmin.lab7.ex3;

import java.io.*;
import java.util.Scanner;

public class EncryptFile {


    public EncryptFile() {
    }

    public void encrypt() {
        File file = new File("myFile.txt");
        Scanner scanner;
        String[] words;
        char[] ch;
        String aux;
        BufferedWriter writer;
        int auxiliar;

        try {
            writer = new BufferedWriter(new FileWriter("data.enc"));
            scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                words = line.split(" ");
                for (String word : words) {
                    ch = word.toCharArray();
                    aux = "";
                    for (int i = 0; i < ch.length; i++) {
                        auxiliar = ch[i] << 1;
                        aux = aux + (char) auxiliar;
                    }
                    writer.write(aux + " ");
                }
                writer.newLine();
            }
            writer.close();
        } catch (FileNotFoundException e) {
            System.out.println("Fisierul nu a fost gasit!");
        } catch (UnsupportedEncodingException e) {
            System.out.println("Unsupported Encoding!");
        } catch (IOException e) {
            System.out.println("IO error!");
        }
    }

    public void decrypt() {
        File file = new File("data.enc");
        BufferedWriter writer;
        String[] cuvinte;
        char[] caractere;
        String aux;
        int auxiliar;

        try {
            writer = new BufferedWriter(new FileWriter("data.dec"));
            Scanner scan = new Scanner(file);
            while (scan.hasNextLine()) {
                String line = scan.nextLine();
                cuvinte = line.split(" ");
                for (String word : cuvinte) {
                    aux = "";
                    caractere = word.toCharArray();
                    for (char ch : caractere) {
                        auxiliar = ch >> 1;
//                        ch[i] = (char) (ch[i] << 1);
                        aux = aux + (char) auxiliar;
                    }
                    writer.write(aux + " ");
                }
                writer.newLine();
            }
            writer.close();
        } catch (FileNotFoundException e) {
            System.out.println("Fisierul nu a fost gasit!");
        } catch (IOException e) {
            System.out.println("IO error!");
        }
    }

}
