package cuciorva.cosmin.lab7.ex3;

import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        EncryptFile encryptFile = new EncryptFile();
        int op;
        Scanner scanner = new Scanner(System.in);
        System.out.println("1.Encrypt\n2.Decrypt");
        System.out.println("Introduceti optiunea dorita:");
        op = scanner.nextInt();
        switch (op) {
            case 1:
                encryptFile.encrypt();
                break;
            case 2:
                encryptFile.decrypt();
                break;
            default:
                System.out.println("Nu este o optiune!");
                return;
        }


    }
}
