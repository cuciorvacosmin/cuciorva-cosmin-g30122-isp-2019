package cuciorva.cosmin.lab7.ex1;

class CofeeMaker {

    Cofee makeCofee() throws MyException  {
        System.out.println("Make a coffe");
        int t = (int) (Math.random() * 100);
        int c = (int) (Math.random() * 100);
        Cofee cofee = new Cofee(t, c);
        if(cofee.contor>10)
            throw new MyException(Cofee.contor,"Not available");
        return cofee;
    }
}