package cuciorva.cosmin.lab7.ex1;

class Cofee {
    private int temp;
    private int conc;
    public static int contor=0;
    Cofee(int t, int c) {
        temp = t;
        conc = c;
        contor++;
    }

    int getTemp() {
        return temp;
    }

    int getConc() {
        return conc;
    }

    public static int getContor() {
        return contor;
    }

    public String toString() {
        return "[cofee temperature=" + temp + ":concentration=" + conc + "]";
    }
}
