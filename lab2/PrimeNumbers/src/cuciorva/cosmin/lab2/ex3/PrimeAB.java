package cuciorva.cosmin.lab2.ex3;
import java.util.Scanner;

public class PrimeAB {
    public static boolean numberPrim(int number) // Functia returneaza doar true sau false - pentru ca nu avem nevoie de alte valori
    {
        if(number < 2) // Daca numarul este mai mic ca si 2 (1, 0, -1, -2, etc) - acesta nu este prim
            return false;
        if(number == 2) // Daca numarul este 2, acesta este prim
            return true;
         for(int i = 2; i <= Math.sqrt(number); i++)
            if(number % i == 0) // Daca acesta se imparte exact la acel numar, inseamna ca nu este prim
                return false;
        return true;
    }
    public  void main(String[] args) {
        Scanner input=new Scanner(System.in);
        int A,B,i,contorNrPrime;
        contorNrPrime=0;
        A=input.nextInt();
        B=input.nextInt();
        if( A < B) {for (i = A + 1; i < B; i++)
            if (numberPrim(i)) contorNrPrime++;}
        else if (B < A){
            for (i = B + 1; i < A; i++)
                if (numberPrim(i)) contorNrPrime++;
        }else System.out.println("A=B;ERROR");
    }
}
