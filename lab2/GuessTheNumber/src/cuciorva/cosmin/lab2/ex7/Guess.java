package cuciorva.cosmin.lab2.ex7;

import java.util.Random;
import java.util.Scanner;

public class Guess {
    public static void main(String[] args) {
        Random rand = new Random();
        Scanner scan = new Scanner(System.in);
        int contor = 0, numberGuess, tryGuessNumber;
        numberGuess = rand.nextInt(99) + 1;
        do {
            System.out.println("Enter the number: ");
            tryGuessNumber = scan.nextInt();
            if (numberGuess < tryGuessNumber) {
                System.out.println("Wrong answear,your number is too high ");
            } else if (numberGuess > tryGuessNumber) {
                System.out.println("Wrong answear,your number is too low");
            } else {
                System.out.println("Congratulations!");
            }
            contor++;
        } while (contor != 7);
        System.out.println("We think at this number: " + numberGuess);
    }
}
