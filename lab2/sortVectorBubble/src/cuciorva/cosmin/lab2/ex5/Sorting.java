package cuciorva.cosmin.lab2.ex5;

import java.util.Scanner;

public class Sorting {
    public  static void swap(int a,int b)
    {
        int aux = a;
        a = b;
        b = aux;
    }
    static void bubbleSort(int list[])
    {
        int n = list.length;
        for (int i = 0; i < n-1; i++)
            for (int j = 0; j < n-i-1; j++)
                if (list[j] > list[j+1])
                {
                    // swap arr[j+1] and arr[i]
                    int temp = list[j];
                    list[j] = list[j+1];
                    list[j+1] = temp;
                }
    }
    public static void main(String[] args) {
       int N;
        Scanner input=new Scanner(System.in);
        System.out.print("N=");
        N=input.nextInt();
        int[] list=new int[N];
        for (int i = 0; i < N; i++){
            System.out.print("list["+i+"]= ");
            list[i]=input.nextInt();
            System.out.println();
        }
        bubbleSort(list);

        for (int i = 0; i <list.length; i++) {
            System.out.println("Sorted list: " + list[i]);
        }
    }
}
