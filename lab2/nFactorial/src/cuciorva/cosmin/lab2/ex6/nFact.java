package cuciorva.cosmin.lab2.ex6;

import java.util.Scanner;

public class nFact {
    static void fact(int n){
        int rez=1;
        for(int i=1; i<=n; i++){
            rez=rez*i;
        }
        System.out.println(n+" Factorial="+rez);
    } // non-recursive
    static  int factorialR (int n)
    {
        if (n==1) return 1;
        else return n*factorialR(n-1);
    }//recursive

    public static void main(String[] args) {

        int N;
        Scanner input=new Scanner(System.in);
        N=input.nextInt();
        fact(N); //non-recursive
        System.out.println("Rezultatul echivalent: "+factorialR(N)); //recursive

    }
}
