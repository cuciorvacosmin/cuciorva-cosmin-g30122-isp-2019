package cuciorva.cosmin.lab2.ex1;

import java.util.Scanner;

public class TwoNumbers {
    public static void main(String[] args) {
        Scanner integerScan = new Scanner(System.in);
        int x,y;
        System.out.println("Enter x:");
        x=integerScan.nextInt();
        System.out.println("Enter y:");
        y=integerScan.nextInt();
        if(x > y){
            System.out.println("Maximum between x and y is x:"+x);
        } else
            System.out.println("Maximum between x and y is y:"+y);

    }
}
