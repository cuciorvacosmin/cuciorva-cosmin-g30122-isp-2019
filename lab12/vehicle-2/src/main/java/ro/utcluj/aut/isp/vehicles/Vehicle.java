package ro.utcluj.aut.isp.vehicles;

import java.util.Objects;

public class Vehicle {

    private String type;
    private int weight;

    public Vehicle(String type, int length) {
        this.type = type;
    }

    @Override
    public boolean equals (Object o) {
        if (this == o) return true;
        if (!(o instanceof Vehicle)) return false;
        Vehicle vehicle = (Vehicle) o;
        return weight == vehicle.weight &&
                Objects.equals (type, vehicle.type);
    }

    public int getWeight () {
        return weight;
    }

    @Override
    public int hashCode () {
        return Objects.hash (type, weight);
    }

    public String start(){
        return "engine started";
    }

}
