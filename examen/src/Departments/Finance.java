package Departments;

import Entities.Accountant;
import Entities.Employee;

import java.util.ArrayList;
import java.util.Scanner;

public class Finance extends Department {

    public Finance(int id, String name) {
        super(id, name);
    }

    ArrayList<Accountant> accountants = new ArrayList<>();

    @Override
    public void addEmployee(Employee e) {
        accountants.add((Accountant) e);
    }

    @Override
    public void removeEmployee(Employee e) {
        accountants.remove(e);
    }

    @Override
    public void updateEmployee(Employee e) {
        Scanner scanner = new Scanner(System.in);
        for (Accountant a : accountants) {
            if (a.equals(e)) {
                System.out.print("Detalii despre actual" + a.getName() + " " + a.getAddress() + " " + a.getId() + " idDepartament=" + a.getDepartmentId() + " lucreaza=" + a.isWorking());
                System.out.println("Modificati id:");
                a.setId(scanner.nextInt());
                System.out.println("Modificati numele:");
                a.setName(scanner.nextLine());
                System.out.println("Modificati adresa:");
                a.setAddress(scanner.nextLine());
                System.out.println("Modificati id-ul departamentului:");
                a.setDepartmentId(scanner.nextInt());
                if (a.isWorking()) {
                    if (scanner.nextBoolean() == true) {
                        System.out.println(a.getName() + "deja munceste!");
                    } else {
                        System.out.println("Modificati activitatea:");
                        a.setWorking(scanner.nextBoolean());
                    }
                } else {
                    if (scanner.nextBoolean() == false) {
                        System.out.println(a.getName() + "e in pauza!");
                    } else {
                        System.out.println("Modificati activitatea:");
                        a.setWorking(scanner.nextBoolean());
                    }
                }
            }
        }
    }

    @Override
    public Employee getEmployee(int id) {
        for (Accountant a : accountants) {
            if (a.getId() == id) {
                return a;
            }
        }
        return null;
    }
}
