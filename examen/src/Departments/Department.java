package Departments;

import Entities.Employee;

abstract public class Department {
    private int id;
    private String name;

    public Department(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    abstract public void addEmployee(Employee e);

    abstract public void removeEmployee(Employee e);

    abstract public void updateEmployee(Employee e);

    abstract public Employee getEmployee(int id);
}
