package Departments;

import Entities.Employee;
import Entities.Progammer;

import java.util.ArrayList;
import java.util.Scanner;

public class IT extends Department {
    ArrayList<Progammer> progammers = new ArrayList<>();

    public IT(int id, String name) {
        super(id, name);
    }


    @Override
    public void addEmployee(Employee e) {
        if ((e.getDepartmentId() == this.getId()) && (this instanceof IT)) {
            progammers.add((Progammer) e);
        }
        if (progammers.size() > 0) {
            for (Progammer p : progammers) {
                System.out.println(p.getName() + " " + p.getDepartmentId());
            }
        }
    }

    @Override
    public void removeEmployee(Employee e) {
        progammers.remove(e);
    }

    @Override
    public void updateEmployee(Employee e) {
        Scanner scanner = new Scanner(System.in);
        for (Progammer p : progammers) {
            if (p.equals(e)) {
                if ((e.getDepartmentId() == this.getId()) && (this instanceof IT)) {
                    System.out.println("Modifica numele");
                    e.setName(scanner.nextLine());
                    System.out.println("Modifica adresa:");
                    e.setAddress(scanner.nextLine());
                    System.out.println("Modifica id:");
                    e.setId(scanner.nextInt());
                }
            }
        }
    }

    @Override
    public Employee getEmployee(int id) {
        for (Progammer p : progammers) {
            if (p.getId() == id) {
                return p;
            }
        }
        return null;
    }
}
