package Entities;

import Actions.LunchBreak;
import Actions.WorkDay;

public abstract class Employee extends Person implements LunchBreak, WorkDay {
    private boolean working;
    private int departmentId;

    public Employee(int id, String name, String address, boolean working, int departmentId) {
        super(id, name, address);
        this.working = working;
        this.departmentId = departmentId;
    }

    public void work() {
        working = true;
    }

    public void relax(){
        working=false;
    }
    public boolean isWorking(){
        return working;
    }

    public void setWorking(boolean working) {
        this.working = working;
    }

    public int getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(int departmentId) {
        this.departmentId = departmentId;
    }
}
