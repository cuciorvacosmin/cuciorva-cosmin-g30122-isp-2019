package Entities;

import Departments.Department;
import Departments.Finance;
import Departments.IT;

import java.util.ArrayList;

public class Company {
    private String name;
    private String address;
    ArrayList<Department> departments = new ArrayList<>();

    public Company(String name, String address) {
        this.name = name;
        this.address = address;
    }
    public void initDepartments(){
        Department d =new IT(4,"Automatica");
        Department d1=new Finance(22,"FSEGA");
        departments.add(d);
        departments.add(d1);
    }

    public Department getDepartments(int id) {
        for (Department d:departments) {
            if(d.getId()==id)
                return d;
        }
        return null;
    }


}
